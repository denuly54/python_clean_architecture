# Python Clean Architecture example

![the clean architecture cheme](clean_code_schema.png "the clean architecture cheme")

## Mind shift

To make it work you have to think about you program from top to bottom.
First you create **Entity** then **Use Case** that literally should be told to you from your business domain
stakeholder. Example: "Village can be accessed by id".
One endpoint can combine many **Use Cases**, example: CreateVillae -> GetVillageByID.
To make you **Handlers** clear we add **Controller** that compose one or more **Use Cases**.
And only after you got **Entity**, **Use Case** and **Controller** you can write your framework dependent
code - handlers(FastApi), db (request to ElasticSearch server)

### Order to create new feature

1. Create Entity
2. Create UseCase
3. Create Controller
4. Create DAL (Data Access Layer)
5. Create Handler

### Folders organization

Tou should not use that folders structure as first instance truth.
It only shows one way to organise you folders in CLEAN ARCHITECTURE way.

```
├── dal <------------------------------------------------ DB, Another API
│         └── storage
│             └── es
│                 └── villages.py
├── domain
│         ├── entities <--------------------------------- Entities
│         │         ├── point.py
│         │         └── village.py
│         └── usecases <--------------------------------- Use Cases
│             └── villages
│                 ├── exceptions.py
│                 └── get_by_id.py
└── server
    ├── api <-------------------------------------------- Web
    │         └── villages_view.py
    └── cotrollers <------------------------------------- Controllers
        └── villages.py
```

## Run local

    uvicorn main:app --reload