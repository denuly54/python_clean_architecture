from fastapi import FastAPI, HTTPException

from src.dal.storage.es.villages import VillagesEsStore
from src.server.api.villages_view import VillageViewModel, VillageResponse
from src.server.cotrollers.villages import VillageController
from src.domain.usecases.villages.exceptions import VillageNotFound
from src.domain.usecases.villages.get_by_id import GetVillageByIDUseCase

app = FastAPI()

villages_es_storage = VillagesEsStore(
    "http://localhost:9200/",
    "villages-alias"
)

village_get_by_id_use_case = GetVillageByIDUseCase(villages_es_storage)

villages_controller = VillageController(village_get_by_id_use_case)


@app.get("/api/villages/{village_id}", response_model=VillageResponse)
async def root(village_id: str):
    try:
        village_entity = villages_controller.get_by_id(village_id)

        village_view_model = VillageViewModel.from_entity(village_entity)

        return VillageResponse(village_view_model)
    except VillageNotFound:
        raise HTTPException(status_code=404, detail="Village not found")
