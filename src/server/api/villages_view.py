from dataclasses import dataclass

from dataclasses_json import dataclass_json

from src.domain.entities.village import Village


@dataclass_json
@dataclass
class PointViewModel:
    latitude: float
    longitude: float


@dataclass
class VillageViewModel:
    id: str
    description: str
    geo_guid: str
    name_legal: str
    name_trade: str
    location: PointViewModel

    @classmethod
    def from_entity(cls, village: Village):
        return cls(
            village.village_id,
            village.description,
            village.geo_guid,
            village.name_legal,
            village.name_trade,
            PointViewModel(village.location.longitude, village.location.latitude),
        )


@dataclass
class VillageResponse:
    answer: VillageViewModel
