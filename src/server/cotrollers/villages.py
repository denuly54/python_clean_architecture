from src.domain.entities.village import Village


class VillageController:
    def __init__(
            self,
            village_get_by_id_use_case
    ) -> None:
        self.village_get_by_id_use_case = village_get_by_id_use_case

    def get_by_id(self, village_id) -> Village:
        return self.village_get_by_id_use_case.execute(village_id)
