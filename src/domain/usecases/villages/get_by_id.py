from src.domain.entities.village import Village


class GetVillageByIDUseCase:
    def __init__(
            self,
            village_repo
    ) -> None:
        self.village_repo = village_repo

    def execute(self, village_id) -> Village:
        return self.village_repo.get_by_id(village_id)
