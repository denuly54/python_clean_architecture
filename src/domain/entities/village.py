from dataclasses import dataclass

from src.domain.entities.point import Point


@dataclass
class Village:
    village_id: str
    description: str
    geo_guid: str
    name_legal: str
    name_trade: str
    location: Point
