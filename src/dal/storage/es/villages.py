import requests
from dataclasses import dataclass
from dataclasses_json import dataclass_json

from src.domain.entities.point import Point
from src.domain.entities.village import Village
from src.domain.usecases.villages.exceptions import VillageNotFound


@dataclass_json
@dataclass
class PointEs:
    lat: float
    lon: float


@dataclass_json
@dataclass
class VillageEs:
    id: str
    description: str
    geo_guid: str
    name_legal: str
    name_trade: str
    location: PointEs

    def make_entity(self) -> Village:
        return Village(
            self.id,
            self.description,
            self.geo_guid,
            self.name_legal,
            self.name_trade,
            Point(self.location.lat, self.location.lon),
        )


class VillagesEsStore:
    def __init__(
            self,
            base_url: str,
            alias: str,
    ) -> None:
        self.base_url = base_url
        self.alias = alias

    def get_by_id(self, village_id) -> Village:
        resp = requests.get(self.base_url + self.alias + '/_doc/' + village_id)

        if resp.status_code != 200:
            raise VillageNotFound

        base_body = resp.json()

        return VillageEs.from_dict(base_body['_source']).make_entity()
